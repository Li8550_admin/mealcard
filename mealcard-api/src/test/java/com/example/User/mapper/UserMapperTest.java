package com.example.User.mapper;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.example.CardDetails.entity.Carddetails;
import com.example.CardDetails.mapper.CarddetailsMapper;
import com.example.Cards.entity.Cards;
import com.example.Cards.entity.CardsCustom;
import com.example.Cards.mapper.CardsMapper;
import com.example.Cards.service.ICardsService;
import com.example.Cards.service.impl.CardsServiceImpl;
import com.example.Student.entity.Student;
import com.example.Suggestions.entity.Suggestions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runner.Runner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTest {



	@Autowired
	CarddetailsMapper carddetailsMapper;

	@Test
	public void insertSugestion() {
		Suggestions suggestions = new Suggestions();
		suggestions.setImportance(2);
		suggestions.setSuggestion("哈哈");
		suggestions.setCarddetailsId(3);
		suggestions.insert();
		System.out.println(suggestions);
	}

	@Autowired
	CardsMapper cardsMapper;
	@Test
	public void insert() {
		// Carddetails carddetails = new Carddetails();
		// Cards cards = new Cards();
		// UpdateWrapper<Cards> cardsUpdateWrapper = new UpdateWrapper<>();
		// cardsUpdateWrapper.set("money",carddetails.getAmount()+carddetails.getMoney());
		// cardsMapper.updateById()
		Long aLong = cardsMapper.updateMoney(1, 20);
		System.out.println(aLong);

	}



	// @Test
	// public void findByName() {
	// 	Student student = new Student();
	// 	QueryWrapper<Student> userQueryWrapper = new QueryWrapper<>();
	// 	userQueryWrapper.like("name","李");
	// 	List<Student> students = student.selectList(userQueryWrapper);
	// 	System.out.println(students);
	// }

	@Test
	public void testAR() {
		Cards cards = new Cards();
		List<Cards> cards1 = cards.selectList(null);
		for (Cards cards2 : cards1) {
			System.out.println(cards2);
		}
		// List<User> users = user.selectList(null);
		// for (User user1 : users) {
		// 	System.out.println(user1);
		// }
	}

}