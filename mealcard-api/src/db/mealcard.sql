/*
 Navicat MySQL Data Transfer

 Source Server         : local8.0
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : mealcard

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 21/04/2023 22:14:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for carddetails
-- ----------------------------
DROP TABLE IF EXISTS `carddetails`;
CREATE TABLE `carddetails`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` decimal(11, 2) NULL DEFAULT NULL,
  `createtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cards_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of carddetails
-- ----------------------------
INSERT INTO `carddetails` VALUES (1, 500.00, '2021-06-09 15:28:59', 1);
INSERT INTO `carddetails` VALUES (2, 300.00, '2021-06-09 15:29:01', 2);
INSERT INTO `carddetails` VALUES (3, 200.00, '2021-06-09 15:29:02', 3);
INSERT INTO `carddetails` VALUES (4, 50.00, '2021-06-09 15:29:04', 3);
INSERT INTO `carddetails` VALUES (7, 300.00, '2021-06-09 15:29:04', 3);
INSERT INTO `carddetails` VALUES (8, 200.00, '2021-06-09 15:29:05', 2);
INSERT INTO `carddetails` VALUES (9, 300.00, '2021-06-09 15:29:06', 1);
INSERT INTO `carddetails` VALUES (10, 300.00, '2021-06-09 15:29:07', 3);
INSERT INTO `carddetails` VALUES (11, 300.00, '2021-06-09 15:29:07', 1);
INSERT INTO `carddetails` VALUES (12, 100.00, '2021-06-09 15:29:08', 2);
INSERT INTO `carddetails` VALUES (13, 100.00, '2021-06-09 15:29:09', 3);
INSERT INTO `carddetails` VALUES (14, 300.00, '2021-06-09 15:29:09', 1);
INSERT INTO `carddetails` VALUES (15, 300.00, '2021-06-09 15:29:10', 2);
INSERT INTO `carddetails` VALUES (16, 300.00, '2021-06-09 15:29:10', 3);
INSERT INTO `carddetails` VALUES (17, 500.00, '2021-06-09 15:29:14', 1);
INSERT INTO `carddetails` VALUES (21, 300.00, '2021-06-10 09:49:02', 2);
INSERT INTO `carddetails` VALUES (22, 500.00, '2021-06-10 09:52:01', 2);
INSERT INTO `carddetails` VALUES (23, 100.00, '2021-06-10 09:53:24', 2);
INSERT INTO `carddetails` VALUES (24, 500.00, '2021-06-10 09:54:45', 2);
INSERT INTO `carddetails` VALUES (25, 500.00, '2021-06-10 09:55:07', 2);
INSERT INTO `carddetails` VALUES (26, 50.00, '2021-06-10 19:09:50', 6);
INSERT INTO `carddetails` VALUES (27, 50.00, '2021-06-10 19:13:13', 7);
INSERT INTO `carddetails` VALUES (28, 100.00, '2021-06-10 19:17:41', 5);
INSERT INTO `carddetails` VALUES (29, 50.00, '2021-06-10 19:19:14', 5);
INSERT INTO `carddetails` VALUES (30, 100.00, '2021-06-10 19:26:30', 7);
INSERT INTO `carddetails` VALUES (31, 50.00, '2021-06-10 19:29:16', 7);
INSERT INTO `carddetails` VALUES (32, 50.00, '2021-06-10 19:30:13', 7);
INSERT INTO `carddetails` VALUES (33, 50.00, '2021-06-10 19:31:01', 5);
INSERT INTO `carddetails` VALUES (34, 50.00, '2021-06-10 19:31:36', 6);
INSERT INTO `carddetails` VALUES (35, 300.00, '2021-06-10 20:39:42', 5);
INSERT INTO `carddetails` VALUES (36, 500.00, '2021-06-10 20:50:06', 8);
INSERT INTO `carddetails` VALUES (37, 300.00, '2021-06-10 20:51:42', 8);
INSERT INTO `carddetails` VALUES (38, 50.00, '2021-06-10 20:52:14', 8);
INSERT INTO `carddetails` VALUES (39, 50.00, '2021-06-10 20:53:49', 8);
INSERT INTO `carddetails` VALUES (40, 50.00, '2021-06-10 21:06:02', 8);
INSERT INTO `carddetails` VALUES (41, 100.00, '2021-06-11 15:34:54', 10);
INSERT INTO `carddetails` VALUES (42, 50.00, '2021-06-11 15:35:07', 9);
INSERT INTO `carddetails` VALUES (43, 50.00, '2021-06-11 15:35:13', 9);

-- ----------------------------
-- Table structure for cards
-- ----------------------------
DROP TABLE IF EXISTS `cards`;
CREATE TABLE `cards`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cardnumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `money` decimal(10, 2) NULL DEFAULT 0.00,
  `activetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(1) NULL DEFAULT 0 COMMENT '1-激活，0-挂失',
  `student_id` int(11) NULL DEFAULT NULL,
  `deleted` int(1) NULL DEFAULT 0 COMMENT '1代表注销，0代表未注销',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of cards
-- ----------------------------
INSERT INTO `cards` VALUES (1, '20024009', 830.00, '2021-06-11 15:26:38', 0, 1, 0);
INSERT INTO `cards` VALUES (2, '20013401', 5.50, '2021-06-10 15:53:12', 0, 2, 0);
INSERT INTO `cards` VALUES (3, '30216001', 523.30, '2021-06-10 16:20:41', 0, 3, 0);
INSERT INTO `cards` VALUES (4, '29759023', 288.80, '2021-06-11 14:20:35', 0, 4, 0);
INSERT INTO `cards` VALUES (5, '20148997', 100.00, '2021-06-10 16:20:46', 0, 5, 0);
INSERT INTO `cards` VALUES (6, '10014875', 256.90, '2021-06-10 16:20:48', 0, 6, 0);
INSERT INTO `cards` VALUES (7, '10348770', 125.70, '2021-06-10 16:20:54', 0, 7, 0);
INSERT INTO `cards` VALUES (8, '10057995', 299.70, '2021-06-10 20:49:17', 0, 8, 0);
INSERT INTO `cards` VALUES (9, '17647854', 356.00, '2021-06-11 15:35:13', 0, 31, 0);
INSERT INTO `cards` VALUES (10, '16548004', 298.00, '2021-06-11 15:34:54', 0, 32, 0);
INSERT INTO `cards` VALUES (11, '18872995', 122.00, '2021-06-11 10:50:52', 0, 33, 0);
INSERT INTO `cards` VALUES (12, '20897809876', 0.00, '2023-04-21 22:13:00', 0, 65, 1);
INSERT INTO `cards` VALUES (13, '20897809876', 0.00, '2023-04-21 22:13:03', 0, 66, 1);
INSERT INTO `cards` VALUES (14, '20897809876', 0.00, '2023-04-21 22:13:05', 0, 64, 1);

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `classes` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `studentnumber` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `birthday` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sex` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES (1, '李复', '纯阳二班', '20180214101', '1999-09-11', '男');
INSERT INTO `student` VALUES (2, '洛风', '纯阳一班', '20190237001', '2005-02-08', '男');
INSERT INTO `student` VALUES (3, '小七', '七秀一班', '20180215028', '2003-01-14', '女');
INSERT INTO `student` VALUES (4, '李忘生', '纯阳一班', '20170215028', '2001-09-29', '男');
INSERT INTO `student` VALUES (5, '于睿', '纯阳三班', '20170123001', '1999-02-08', '女');
INSERT INTO `student` VALUES (6, '草雪阳', '天策一班', '20201206011', '2001-09-12', '女');
INSERT INTO `student` VALUES (7, '叶凡', '藏剑一班', '20176102029', '1998-11-12', '男');
INSERT INTO `student` VALUES (8, '深剑心', '纯阳四班', '20189107502', '2002-02-03', '男');
INSERT INTO `student` VALUES (30, '杨宁', '天策三班', '20896004345', '2000-06-08', '男');
INSERT INTO `student` VALUES (31, '朱剑秋', '天策四班', '38854002234', '2000-06-06', '男');
INSERT INTO `student` VALUES (32, '叶芷青', '天策二班', '30012663452', '2001-06-07', '女');
INSERT INTO `student` VALUES (33, '莫雨', '侠士一班', '30046009653', '2002-06-07', '男');
INSERT INTO `student` VALUES (34, '穆玄英', '侠士二班', '20087004341', '2001-06-15', '男');
INSERT INTO `student` VALUES (35, '源明雅', '侠士三班', '20013031452', '1998-06-07', '男');
INSERT INTO `student` VALUES (36, '谢云流', '侠士四班', '29975001255', '2001-06-15', '男');
INSERT INTO `student` VALUES (37, '王遗风', '侠士一班', '20075001235', '2021-06-15', '男');
INSERT INTO `student` VALUES (38, '谢渊', '侠士二班', '20013009234', '2001-06-09', '男');
INSERT INTO `student` VALUES (39, '裴元', '万秀三班', '29980664123', '2001-06-14', '男');
INSERT INTO `student` VALUES (40, '陈元', '侠士五班', '28874487345', '2021-06-22', '男');

-- ----------------------------
-- Table structure for suggestions
-- ----------------------------
DROP TABLE IF EXISTS `suggestions`;
CREATE TABLE `suggestions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `suggestion` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `importance` int(11) NOT NULL,
  `carddetails_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of suggestions
-- ----------------------------
INSERT INTO `suggestions` VALUES (10, '酸溜土豆丝一点都酸，味道真不行/(ㄒoㄒ)/~~', 1, 16);
INSERT INTO `suggestions` VALUES (11, '番茄炒蛋真的是鸡蛋味的炒番茄', 1, 17);
INSERT INTO `suggestions` VALUES (15, '菠萝炒饭挺不错o(*￣▽￣*)ブ', 2, 21);
INSERT INTO `suggestions` VALUES (18, '麻烦多给点青菜，肉少可以，但是菜也少是真的无法理解', 3, 24);
INSERT INTO `suggestions` VALUES (19, '有点贵，菜给的又少┭┮﹏┭┮', 2, 35);
INSERT INTO `suggestions` VALUES (20, '冲冲冲，糖醋鱼先到先得', 2, 36);
INSERT INTO `suggestions` VALUES (21, '嗦粉啦，螺蛳粉，老友粉，卤菜粉，生榨粉，猪脚粉，好多好多粉O(∩_∩)O', 2, 37);
INSERT INTO `suggestions` VALUES (22, '┭┮﹏┭┮，干饭都快吃穷了怎么办', 2, 38);
INSERT INTO `suggestions` VALUES (23, '越来越贵，可能泡面是我最后的倔强啊{{{(>_<)}}}', 2, 39);

SET FOREIGN_KEY_CHECKS = 1;
