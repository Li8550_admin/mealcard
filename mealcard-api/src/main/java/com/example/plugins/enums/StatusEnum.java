package com.example.plugins.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;

public enum StatusEnum implements IEnum<Integer> {
	正常(0,"正常"),
	挂失(1,"挂失");

	private int value;
	private String desc;

	StatusEnum (int value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	@Override
	public Integer getValue() {
		return this.value;
	}

	@Override
	public String toString() {return  this.desc;}
}
