package com.example.Student.mapper;

import com.example.Student.entity.Student;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author BlueSky
 * @since 2021-06-04
 */


public interface StudentMapper extends BaseMapper<Student> {

	// 分页查询
	List<Student> findByPage(@Param("start") Integer start, @Param("rows") Integer rows);

	//查询总条数
	Long findTotals();


	// // 分页查询(findByName)
	List<Student> findByName(@Param("studentName") String studentName,
							 @Param("start") Integer start, @Param("rows") Integer rows);
	//
	//查询总条数(fingdByName)
	Long findTotalsByName(@Param("studentName") String studentName);

}
