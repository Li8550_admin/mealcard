package com.example.Student.entity;

    import java.io.Serializable;

    import com.baomidou.mybatisplus.annotation.IdType;
    import com.baomidou.mybatisplus.annotation.TableField;
    import com.baomidou.mybatisplus.annotation.TableId;
    import com.baomidou.mybatisplus.extension.activerecord.Model;
    import lombok.AllArgsConstructor;
    import lombok.Data;
    import lombok.EqualsAndHashCode;
    import lombok.NoArgsConstructor;
    import lombok.experimental.Accessors;

/**
* <p>
    * 
    * </p>
*
* @author BlueSky
* @since 2021-06-04
*/
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @EqualsAndHashCode(callSuper = false)
    @Accessors(chain = true)
    public class Student extends Model<Student> implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private int id;

    @TableField(value = "name")
    private String studentName;

    private String birthday;

    private String classes;

    private String sex;

    @TableField(value = "studentnumber")
    private String studentNumber;

}
