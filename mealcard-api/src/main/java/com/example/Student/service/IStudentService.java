package com.example.Student.service;

import com.example.Student.entity.Student;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.vo.Result;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author BlueSky
 * @since 2021-06-04
 */
public interface IStudentService extends IService<Student> {
	//分页查询
	List<Student> findByPage(Integer pageNow, Integer rows);

	//查询总条数
	Long findTotals();

	//使用AR插件删除
	void delete (Integer id);

	List<Student> findAlls();

	Result saves(Student student);

	List<Student> findByName(String studentName,Integer pageNow, Integer rows);

	Long findTotalsByName(String studentName);
}
