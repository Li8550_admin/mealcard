package com.example.Student.service.impl;

import com.example.Cards.entity.Cards;
import com.example.Cards.mapper.CardsMapper;
import com.example.Student.entity.Student;
import com.example.Student.mapper.StudentMapper;
import com.example.Student.service.IStudentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.plugins.enums.StatusEnum;
import com.example.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author BlueSky
 * @since 2021-06-04
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService {

	@Autowired
	CardsMapper cardsMapper;

	@Override
	public List<Student> findByPage(Integer pageNow, Integer rows) {
		int start = (pageNow-1) * rows;
		return this.baseMapper.findByPage(start,rows);
	}

	@Override
	public Long findTotals() {
		return this.baseMapper.findTotals();
	}

	//使用AR完成CRUD
	@Override
	public void delete(Integer id) {
		Student student = new Student();
		student.setId(id);
		student.deleteById();
	}

	@Override
	public List<Student> findAlls() {
		Student student = new Student();
		List<Student> students = student.selectList(null);
		return students;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Result saves(Student student) {
		Result result = new Result();
		try {
			this.baseMapper.insert(student);
			//student.insert();
			Cards cards = new Cards();
			cards.setDeleted(0);
			cards.setStudentId(student.getId());
			cards.setCardnumber(student.getStudentNumber());
			cardsMapper.insert(cards);

			result.setMsg("学生信息保存成功！");
		}catch (Exception e){
			result.setStatus(false);
			result.setMsg("系统错误：保存用户信息失败，请稍后再试...");

		}
		return result;
	}

	@Override
	public List<Student> findByName(String studentName,Integer pageNow, Integer rows) {
		int start = (pageNow-1) * rows;
		return this.findByName(studentName,start,rows);
	}

	@Override
	public Long findTotalsByName(String studentName) {
		return this.baseMapper.findTotalsByName(studentName);
	}

}
