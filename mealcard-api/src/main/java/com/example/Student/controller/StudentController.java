package com.example.Student.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.Student.entity.Student;
import com.example.Student.service.IStudentService;
import com.example.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author BlueSky
 * @since 2021-06-04
 */
@CrossOrigin
@RestController
@RequestMapping("/student")
public class StudentController {
	@Autowired
	IStudentService studentService;
	// 搜索
	@GetMapping("/findByName")
	public Map<String ,Object> findByName(String studentName,Integer pageNow, Integer pageSize) {
		Map<String,Object> result = new HashMap<>();
		pageNow = pageNow==null?1:pageNow;
		pageSize = pageSize==null?4:pageSize;
		List<Student> students = studentService.findByName(studentName,pageNow, pageSize);
		Long totals = studentService.findTotalsByName(studentName);
		result.put("students",students);
		result.put("total",totals);
		return result;
	}
	// 分页查询
	@GetMapping("/findByPage")
	public Map<String ,Object> findByPage(Integer pageNow, Integer pageSize){
		Map<String,Object> result = new HashMap<>();
		pageNow = pageNow==null?1:pageNow;
		pageSize = pageSize==null?4:pageSize;
		List<Student> students = studentService.findByPage(pageNow, pageSize);
		Long totals = studentService.findTotals();
		result.put("students",students);
		result.put("total",totals);
		return result;
	}
	//学生信息列表
	// @GetMapping("/findAlls")
	// public List<Student> findAlls() throws JsonProcessingException {
	// 	return studentService.findAlls();
	// }

	// 删除
	@GetMapping("/deletes")
	public Result delete(Integer id)  {
		Result result = new Result();
		try {
			studentService.delete(id);
			result.setMsg("删除学生信息成功！");
		}catch (Exception e){
			e.printStackTrace();
			result.setStatus(false);
			result.setMsg("删除学生信息失败，请稍后再试！");
		}
		return result;
	}
	// 添加
	@PostMapping("/save")
	public Result save(@RequestBody Student student) {
		return studentService.saves(student);
	}
	//更新
	@PostMapping("/update")
	public Result update(@RequestBody Student student) {
		Result result = new Result();
		try {
			Student student1=student;
			student1.updateById();
			result.setMsg("修改成功！");
		}catch (Exception e){
			result.setStatus(false);
			result.setMsg("系统错误：保存学生信息失败，请稍后再试...");
		}
		return result;
	}

}
