package com.example.CardDetails.mapper;

import com.example.CardDetails.entity.Carddetails;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.CardDetails.entity.CarddetailsCustom;
import com.example.Cards.entity.Cards;
import com.example.Suggestions.entity.SuggestionsCustom;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author BlueSky
 * @since 2021-06-07
 */
public interface CarddetailsMapper extends BaseMapper<Carddetails> {

}
