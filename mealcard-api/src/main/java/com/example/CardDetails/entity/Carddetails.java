package com.example.CardDetails.entity;

    import java.math.BigDecimal;
    import java.time.LocalDateTime;
    import java.io.Serializable;

    import com.baomidou.mybatisplus.annotation.IdType;
    import com.baomidou.mybatisplus.annotation.TableField;
    import com.baomidou.mybatisplus.annotation.TableId;
    import com.baomidou.mybatisplus.extension.activerecord.Model;
    import lombok.AllArgsConstructor;
    import lombok.Data;
    import lombok.EqualsAndHashCode;
    import lombok.NoArgsConstructor;
    import lombok.experimental.Accessors;

/**
* <p>
    * 
    * </p>
*
* @author BlueSky
* @since 2021-06-07
*/
    @Data
        @EqualsAndHashCode(callSuper = false)
    @Accessors(chain = true)
    @AllArgsConstructor
    @NoArgsConstructor
    public class Carddetails extends Model<Carddetails> implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private int id;

    private int amount;

    @TableField(value = "cards_id")
    private int cardId;

    private String createtime;

    @TableField(exist = false)
    private Integer importance;

    @TableField(exist = false)
    private String suggestion;

}
