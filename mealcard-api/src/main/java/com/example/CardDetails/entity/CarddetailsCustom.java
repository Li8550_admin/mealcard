package com.example.CardDetails.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarddetailsCustom extends Carddetails {
	private String studentName;
}
