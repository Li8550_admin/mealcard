package com.example.CardDetails.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.CardDetails.entity.Carddetails;
import com.example.CardDetails.entity.CarddetailsCustom;
import com.example.CardDetails.service.ICarddetailsService;
import com.example.Cards.entity.Cards;
import com.example.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author BlueSky
 * @since 2021-06-07
 */
@CrossOrigin
@RestController
@RequestMapping("/carddetails")
public class CarddetailsController {

	@Autowired
	ICarddetailsService carddetailsService;

	@GetMapping("/findByCardId")
	public List<Carddetails> findByName(String cardId) {
		Carddetails carddetails = new Carddetails();
		QueryWrapper<Carddetails> carddetailsQueryWrapper = new QueryWrapper<>();
		carddetailsQueryWrapper.like("cards_id",cardId);
		List<Carddetails> carddetails1 = carddetails.selectList(carddetailsQueryWrapper);

		return carddetails1;
	}
	// 添加
	@PostMapping("/save")
	public Result saves(@RequestBody Carddetails carddetails) {
		return carddetailsService.saves(carddetails);
	}

}
