package com.example.CardDetails.service.impl;

import com.example.CardDetails.entity.Carddetails;
import com.example.CardDetails.entity.CarddetailsCustom;
import com.example.CardDetails.mapper.CarddetailsMapper;
import com.example.CardDetails.service.ICarddetailsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.Cards.entity.Cards;
import com.example.Cards.mapper.CardsMapper;
import com.example.Suggestions.entity.Suggestions;
import com.example.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author BlueSky
 * @since 2021-06-07
 */
@Service
public class CarddetailsServiceImpl extends ServiceImpl<CarddetailsMapper, Carddetails> implements ICarddetailsService {


	@Autowired
	CardsMapper cardsMapper;

	@Override
	public Result saves(Carddetails carddetails) {
		Result result = new Result();
		try {
			// System.out.println(carddetails.getAmount()+carddetails.getMoney());

			// System.out.println(carddetails.getCardId());
			// 1.先把充值金额加到余额上
			Long aLong = cardsMapper.updateMoney(carddetails.getCardId(), carddetails.getAmount());
			// cards.setMoney(money);
			// 2.添加充值记录
			if (aLong!=0){
				carddetails.insert();
			}
			// 3.添加建议数据到库里(空建议不添加)
			if (carddetails.getSuggestion()==null){

			}else {
				Suggestions suggestions = new Suggestions();
				suggestions.setCarddetailsId(carddetails.getId());
				suggestions.setImportance(carddetails.getImportance());
				suggestions.setSuggestion(carddetails.getSuggestion());
				suggestions.insert();
			}
			result.setMsg("充值成功！");
		}catch (Exception e){
			result.setStatus(false);
			result.setMsg("系统错误：充值失败，请稍后再试...");

		}
		return result;
	}



}
