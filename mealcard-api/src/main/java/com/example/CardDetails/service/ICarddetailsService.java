package com.example.CardDetails.service;

import com.example.CardDetails.entity.Carddetails;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.CardDetails.entity.CarddetailsCustom;
import com.example.vo.Result;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author BlueSky
 * @since 2021-06-07
 */
public interface ICarddetailsService extends IService<Carddetails> {

	Result saves(Carddetails carddetails);
}
