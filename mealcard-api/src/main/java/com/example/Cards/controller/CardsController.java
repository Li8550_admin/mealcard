package com.example.Cards.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.Cards.entity.Cards;
import com.example.Cards.entity.CardsCustom;
import com.example.Cards.service.ICardsService;
import com.example.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author BlueSky
 * @since 2021-06-06
 */
@CrossOrigin
@RestController
@RequestMapping("/cards")
public class CardsController {

	@Autowired
	ICardsService cardsService;

	// 分页查询
	@GetMapping("/findByPage")
	public Map<String ,Object> findByPage(Integer pageNow, Integer pageSize){
		Map<String,Object> result = new HashMap<>();
		pageNow = pageNow==null?1:pageNow;
		pageSize = pageSize==null?4:pageSize;
		List<CardsCustom> CardsCustom = cardsService.findByPage(pageNow, pageSize);
		Long totals = cardsService.findTotals();
		result.put("cards",CardsCustom);
		result.put("total",totals);
		return result;
	}

	// 注销（逻辑删除）
	@GetMapping("/delete")
	public Result delete(Integer id)  {
		Result result = new Result();
		try {
			Cards cards = new Cards();
			cards.deleteById(id);
			result.setMsg("注销成功！");
		}catch (Exception e){
			e.printStackTrace();
			result.setStatus(false);
			result.setMsg("删除用户信息失败，请稍后再试！");
		}
		return result;
	}
}
