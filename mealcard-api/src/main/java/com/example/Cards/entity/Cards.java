package com.example.Cards.entity;

    import java.math.BigDecimal;
    import java.time.LocalDateTime;
    import java.io.Serializable;
    import java.util.Date;

    import com.baomidou.mybatisplus.annotation.IdType;
    import com.baomidou.mybatisplus.annotation.TableId;
    import com.baomidou.mybatisplus.annotation.TableLogic;
    import com.baomidou.mybatisplus.extension.activerecord.Model;
    import com.example.plugins.enums.StatusEnum;
    import lombok.AllArgsConstructor;
    import lombok.Data;
    import lombok.EqualsAndHashCode;
    import lombok.NoArgsConstructor;
    import lombok.experimental.Accessors;

/**
* <p>
    *
    * </p>
*
* @author BlueSky
* @since 2021-06-06
*/
    @Data
        @EqualsAndHashCode(callSuper = false)
    @Accessors(chain = true)
    @AllArgsConstructor
    @NoArgsConstructor
    public class Cards extends Model<Cards> implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String cardnumber;

    private BigDecimal money;

    private StatusEnum status;

    private String activetime;

    private Integer studentId;

    @TableLogic
    private Integer deleted;
}
