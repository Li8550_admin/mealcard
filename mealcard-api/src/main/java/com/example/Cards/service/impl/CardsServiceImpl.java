package com.example.Cards.service.impl;

import com.example.Cards.entity.Cards;
import com.example.Cards.entity.CardsCustom;
import com.example.Cards.mapper.CardsMapper;
import com.example.Cards.service.ICardsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author BlueSky
 * @since 2021-06-06
 */
@Service
public class CardsServiceImpl extends ServiceImpl<CardsMapper, Cards> implements ICardsService {
	@Autowired
	CardsMapper cardsMapper;

	@Override
	public List<CardsCustom> findByPage(Integer pageNow, Integer rows) {
		int start = (pageNow-1) * rows;
		return cardsMapper.findByPage(start,rows);
	}

	@Override
	public Long findTotals() {
		return cardsMapper.findTotals();
	}


	@Override
	public Long updateMoney(Integer id, Integer amount) {
		return cardsMapper.updateMoney(id,amount);
	}
}
