package com.example.Cards.service;

import com.baomidou.mybatisplus.core.injector.methods.UpdateById;
import com.example.Cards.entity.Cards;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.Cards.entity.CardsCustom;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author BlueSky
 * @since 2021-06-06
 */
public interface ICardsService extends IService<Cards> {
	//分页查询
	List<CardsCustom> findByPage(Integer pageNow, Integer rows);

	//查询总条数
	Long findTotals();

	Long updateMoney (Integer id,Integer amount);

}
