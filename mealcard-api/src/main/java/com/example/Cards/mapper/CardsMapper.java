package com.example.Cards.mapper;

import com.example.Cards.entity.Cards;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.Cards.entity.CardsCustom;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author BlueSky
 * @since 2021-06-06
 */
public interface CardsMapper extends BaseMapper<Cards> {

	// 分页查询
	List<CardsCustom> findByPage(@Param("start") Integer start, @Param("rows") Integer rows);

	//查询总条数
	Long findTotals();



	Long updateMoney(Integer id, Integer amount);
}
