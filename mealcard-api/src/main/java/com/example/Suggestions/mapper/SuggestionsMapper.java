package com.example.Suggestions.mapper;

import com.example.Student.entity.Student;
import com.example.Suggestions.entity.Suggestions;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.Suggestions.entity.SuggestionsCustom;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author BlueSky
 * @since 2021-06-05
 */
public interface SuggestionsMapper extends BaseMapper<Suggestions> {
	// 分页查询
	List<SuggestionsCustom> findByPage(@Param("start") Integer start, @Param("rows") Integer rows);

	//查询总条数
	Long findTotals();
}
