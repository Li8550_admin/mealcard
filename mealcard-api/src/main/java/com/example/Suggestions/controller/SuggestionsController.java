package com.example.Suggestions.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.Student.entity.Student;
import com.example.Student.service.IStudentService;
import com.example.Suggestions.entity.Suggestions;
import com.example.Suggestions.entity.SuggestionsCustom;
import com.example.Suggestions.service.ISuggestionsService;
import com.example.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author BlueSky
 * @since 2021-06-05
 */
@CrossOrigin
@RestController
@RequestMapping("/suggestions")
public class SuggestionsController {

	@Autowired
	ISuggestionsService suggestionsService;
	// // 搜索
	// @GetMapping("/findByName")
	// public List<Suggestions> findByName(String userName) {
	// 	Suggestions suggestions = new Suggestions();
	// 	QueryWrapper<Suggestions> userQueryWrapper = new QueryWrapper<>();
	// 	userQueryWrapper.like("studentname",userName);
	// 	List<Suggestions> suggestions1 = suggestions.selectList(userQueryWrapper);
	// 	return suggestions1;
	// }
	// 分页查询
	@GetMapping("/findByPage")
	public Map<String ,Object> findByPage(Integer pageNow, Integer pageSize){
		Map<String,Object> result = new HashMap<>();
		pageNow = pageNow==null?1:pageNow;
		pageSize = pageSize==null?4:pageSize;
		List<SuggestionsCustom> suggestionsCustoms = suggestionsService.findByPage(pageNow, pageSize);
		Long totals = suggestionsService.findTotals();
		result.put("suggestions",suggestionsCustoms);
		result.put("total",totals);
		return result;
	}
	//学生信息列表
	// @GetMapping("/findAlls")
	// public List<Student> findAlls() throws JsonProcessingException {
	// 	return studentService.findAlls();
	// }

	// 删除
	@GetMapping("/delete")
	public Result delete(Integer id)  {
		Result result = new Result();
		try {
			suggestionsService.delete(id);
			result.setMsg("删除成功！");
		}catch (Exception e){
			e.printStackTrace();
			result.setStatus(false);
			result.setMsg("删除失败，请稍后再试！");
		}
		return result;
	}
	// 添加
	// @PostMapping("/save")
	// public Result save(@RequestBody Student student) {
	// 	Result result = new Result();
	// 	try {
	// 		Student student1=student;
	// 		student1.insert();
	// 		result.setMsg("用户信息保存成功！");
	// 	}catch (Exception e){
	// 		result.setStatus(false);
	// 		result.setMsg("系统错误：保存用户信息失败，请稍后再试...");
	//
	// 	}
	// 	return result;
	// }

}
