package com.example.Suggestions.service.impl;

import com.example.Student.entity.Student;
import com.example.Student.mapper.StudentMapper;
import com.example.Suggestions.entity.Suggestions;
import com.example.Suggestions.entity.SuggestionsCustom;
import com.example.Suggestions.mapper.SuggestionsMapper;
import com.example.Suggestions.service.ISuggestionsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author BlueSky
 * @since 2021-06-05
 */
@Service
public class SuggestionsServiceImpl extends ServiceImpl<SuggestionsMapper, Suggestions> implements ISuggestionsService {

	@Autowired
	SuggestionsMapper suggestionsMapper;

	@Override
	public List<SuggestionsCustom> findByPage(Integer pageNow, Integer rows) {
		int start = (pageNow-1) * rows;
		return suggestionsMapper.findByPage(start,rows);
	}

	@Override
	public Long findTotals() {
		return suggestionsMapper.findTotals();
	}

	//使用AR完成CRUD
	@Override
	public void delete(Integer id) {
		Suggestions suggestions = new Suggestions();
		suggestions.deleteById(id);

	}

}
