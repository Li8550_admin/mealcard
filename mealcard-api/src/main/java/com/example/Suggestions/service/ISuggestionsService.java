package com.example.Suggestions.service;

import com.example.Student.entity.Student;
import com.example.Suggestions.entity.Suggestions;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.Suggestions.entity.SuggestionsCustom;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author BlueSky
 * @since 2021-06-05
 */
public interface ISuggestionsService extends IService<Suggestions> {

	//分页查询
	List<SuggestionsCustom> findByPage(Integer pageNow, Integer rows);

	//查询总条数
	Long findTotals();

	//使用AR插件删除
	void delete (Integer id);

}
