package com.example.Suggestions.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SuggestionsCustom extends Suggestions {

	private String studentName;

	private String createtime;


}
